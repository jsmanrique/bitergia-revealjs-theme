# Bitergia theme for reveal.js

This repository contains a [.css file](css/bitergia.css) created for 
[reveal.js](https://github.com/hakimel/reveal.js) presentations to have a *Bitergian* style 
template for presentations.

![screenshot](screenshot.jpg)

## How to use it

First of all, you need to get [reveal.js](https://github.com/hakimel/reveal.js/). This repository includes `reveal.js` 
as [git submodule](https://git-scm.com/book/en/v2/Git-Tools-Submodules), so you can get it using git magic:

1. Run `git submodule init` to initialize your local configuration file
```bash
bitergia-revealjs-theme $ git submodule init
Submodule 'reveal.js' (https://github.com/hakimel/reveal.js) registered for path 'reveal.js'
bitergia-revealjs-theme $
```

1. Run `git submodule update` to fetch all the data from that project and check out the appropriate commit
```bash
bitergia-revealjs-theme $ git submodule update
Clone in «/tmp/bitergia-revealjs-theme/reveal.js»...
Submodule path «reveal.js»: checked out «a0c013606e130baad976195730dcd7e7cd9e2855»
```

There are a couple of files ready to be used:
* [slides.html](slides.html), that is the core html to do the presentation.
* [example.md](example.md), as slides content in markdown. You can use direct html in `slides.html` if you prefer.

You can write your own `.html` file for the presentation and it needs to have `bitergia.css` included. For example:

```html
<!-- Bitergia custom theme -->
<link rel="stylesheet" href="./css/bitergia.css">
```

To add Bitergia's logo on the bottom left, add `bitergia` class to `body` element:
```html
<body class="bitergia">
```

To add both, Bitergia's logo and Creative Commons *by-sa* logo, add
`bitergia-cc` class:
```html
<body class="bitergia-cc">
```


# How to create or modify the theme

You can create your own theme files very easy with reveal.js

1. Fetch reveal.js module
1. Install dependencies
```bash
$ cd reveal.js
reveal.js$ npm install
```

1. In `css/theme/source` create a `.scss` file or modify one of the existing ones
1. Create the final `.css` file with `grunt css-themes`:
```bash
reveal.js$ grunt css-themes
```

1. The new file should be in `css/theme/`

# License

MIT licensed

Copyright (C) 2017 J. Manrique López, http://jsmanrique.es